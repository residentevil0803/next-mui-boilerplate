import { setCookie } from 'nookies'
import { call, put } from 'redux-saga/effects'

import { userApi } from '../../api/user.api'
import { COOKIES } from '../../utility'

import {
  logInSuccess,
  logInFailure,

  getMeSuccess,
  getMeFailure
} from '../slice/userSlice'

type TLogdata = {
  token: string
}

export function* logInSaga(action: any) {
  try {
    const data: TLogdata = yield call(userApi.logIn, action.payload)
    if (data) {
      yield call(setCookie, null, COOKIES.JWT, data.token, { maxAge: 8640 })
      yield put(logInSuccess(data));
    }
  } catch (error) {
    yield put(logInFailure(error))
  }
}

export function* getMeSaga(action: any) {
  try {
    const { data } = yield call(userApi.getMe, action.payload)
    if (data) {
      yield put(getMeSuccess(data))
    }
  } catch (error) {
    yield put(getMeFailure(error))
  }
}