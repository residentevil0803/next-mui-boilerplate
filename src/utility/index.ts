import createEmotionCache from "./createEmotionCache"
import PATHS from "./path"
import COOKIES from "./cookies"

const PUBLIC_PATHS = [PATHS.LOGIN]

export {
  createEmotionCache,
  PATHS,
  PUBLIC_PATHS,
  COOKIES
}