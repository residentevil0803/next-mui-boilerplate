import type { NextPage } from 'next'
import Link from 'next/link'

import { useState, useEffect, useRef } from 'react'
import { useRouter } from 'next/router'
import { useDispatch, useSelector } from 'react-redux'

import { DashboardStyle } from '../../styles/pages/dashboard/index.style'

const DashboardPage: NextPage = () => {
  return (
    <DashboardStyle>
      Dashboard Page
    </DashboardStyle>
  )
}

export default DashboardPage