import React, { useState, useEffect } from "react";

import {
  FormControl,
  FormLabel,
  OutlinedInput,
  InputAdornment,
  Typography,
  IconButton,
} from "@mui/material";

import { InputStyle } from "./index.style";

type Props = {
  label: string;
  value: string;
  type: string;
  inputRef: any;
};

const InputComponent: React.FC<Props> = ({ label, value, type, inputRef }) => {
  const [validationError, setValidationError] = useState<boolean>(false);
  const [inputValue, setInputValue] = useState<string>("");

  useEffect(() => {
    setInputValue(value);
  }, [value]);

  return (
    <InputStyle>
      <FormControl error={validationError} fullWidth>
        {label && (
          <FormLabel className="label-container">
            * <Typography className="label-show"> {label} </Typography>
          </FormLabel>
        )}

        <OutlinedInput
          className="input-container"
          inputRef={inputRef}
          defaultValue={inputValue}
          type={type}
          autoComplete="off"
        />
      </FormControl>
    </InputStyle>
  );
};

export default InputComponent;
