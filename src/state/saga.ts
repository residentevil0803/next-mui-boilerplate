import { all, takeLatest } from 'redux-saga/effects'

import {
  logInSaga,
  getMeSaga
} from './saga/userSaga'

import {
  logIn,
  getMe
} from './slice/userSlice'

function* rootSaga() {
  yield all([
    takeLatest(logIn.type, logInSaga),
    takeLatest(getMe.type, getMeSaga),
  ])
}

export default rootSaga