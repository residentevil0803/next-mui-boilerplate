// Import customized themes
import lightTheme from './theme/lightTheme'

export {
  lightTheme
}