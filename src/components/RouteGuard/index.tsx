import { useRouter } from 'next/router'
import { parseCookies } from 'nookies'
import { useState, useEffect } from 'react'

import { COOKIES, PATHS, PUBLIC_PATHS } from '../../utility'

const RouteGuard: React.FC<any> = ({ children }: any) => {
  const router = useRouter()
  const [authorized, setAuthorized] = useState(false)

  useEffect(() => {
    authCheck(router.asPath)

    const hideContent = () => setAuthorized(false)
    router.events.on('routeChangeStart', hideContent)
    router.events.on('routeChangeComplete', authCheck)

    return () => {
      router.events.off('routeChangeStart', hideContent)
      router.events.off('routeChangeComplete', authCheck)
    }
  }, [])

  async function authCheck(url: string) {
    const cookies = parseCookies()
    // redirect to login page if accessing a private page and not logged in
    const path = url.split('?')[0]
    if (!cookies[COOKIES.JWT] && !PUBLIC_PATHS.includes(path)) {
      setAuthorized(false)
      router.push({
        pathname: PATHS.LOGIN
      })
    } else {
    setAuthorized(true)
    }
  }

  return children
}

export default RouteGuard