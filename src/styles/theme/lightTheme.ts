import { createTheme } from '@mui/material/styles'

const themeColors = {
  text: {
    primary: '#000000',
    white: '#FFFFFF',
    accentBlue: '#3937DE',
  },
  bg: {
    primary: '#FFFFFF',
    blue: '#30A7FF'
  }
} as const

const lightTheme = createTheme({
  ...themeColors,
  palette: {
    mode: 'light'
  }
})

export default lightTheme