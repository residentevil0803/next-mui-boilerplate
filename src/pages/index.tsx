import type { NextPage } from "next";
import Link from "next/link";

import { useState, useEffect, useRef } from "react";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";

import { logIn } from "../state/slice/userSlice";
import { PATHS } from "../utility";

import {
  Box,
  Button,
  Typography,
  FormGroup,
  FormControlLabel,
  Checkbox,
} from "@mui/material";

import { InputComponent } from "../components";
import { LoginStyle } from "./index.style";
import { RootState } from "../state/store";

const LoginPage: NextPage = () => {
  const dispatch = useDispatch();
  const router = useRouter();
  const { user, loggedIn, loggingIn } = useSelector(
    (state: RootState) => state.user
  );

  const [isLogging, setIsLogging] = useState<boolean>(false);

  const emailRef = useRef() as React.MutableRefObject<HTMLInputElement>;
  const passwordRef = useRef() as React.MutableRefObject<HTMLInputElement>;
  const adminLoginRef = useRef() as React.MutableRefObject<HTMLInputElement>;

  const onLogInClick = () => {
    const loginData = {
      email: emailRef.current.value,
      password: passwordRef.current.value,
      adminLogin: adminLoginRef.current.checked,
    };
    dispatch(logIn(loginData));
    setIsLogging(true);
  };

  useEffect(() => {
    if (loggedIn === true) {
      alert("You logged in successfully!");
      router.push(PATHS.DASHBOARD);
    } else if (isLogging === true && loggingIn === false) {
      setIsLogging(false);
      alert("Unmatched input data. Try again!");
    }
  }, [dispatch, loggedIn, loggingIn]);

  return (
    <LoginStyle>
      <Box className="title-container">BYFROST</Box>
      <Box className="login-container">
        <Box className="input-box">
          <InputComponent
            inputRef={emailRef}
            label="Email address"
            value=""
            type="text"
          />
        </Box>
        <Box className="input-box">
          <InputComponent
            inputRef={passwordRef}
            label="Password"
            value=""
            type="password"
          />
        </Box>

        <FormGroup>
          <FormControlLabel
            control={<Checkbox inputRef={adminLoginRef} />}
            label="Log in as admin"
          />
        </FormGroup>

        <Button onClick={onLogInClick} className="login-button-container">
          Log In
        </Button>

        <Box className="notice-container" sx={{ justifyContent: "center" }}>
          <Typography className="notice-title"> No account yet? </Typography>
          <Link className="link-input" href={""}>
            {" "}
            Sign up{" "}
          </Link>
        </Box>
      </Box>
    </LoginStyle>
  );
};

export default LoginPage;
