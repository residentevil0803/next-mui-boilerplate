import { getClient } from "./client";

const mainClient = getClient(process.env.NEXT_PUBLIC_API || '')

export const userApi = {
  async logIn(data: any) {
    try {
      console.log("Data: ", data)
      const res = await mainClient.post('/api/user/login', data)
      console.log("sdfs : ", res)
      const resBody = res.data
      return resBody
      // const res = await axios.post('http://localhost:3000/api/user/login', data);
    } catch (error) {
      throw error
    }
  },

  async getMe(data: any) {
    try {
      const res = await mainClient.post('', data)
      const resBody = res.data
      return resBody
    } catch (error) {
      throw error
    }
  }
}