import { EmotionCache } from '@emotion/cache'
import { CacheProvider } from '@emotion/react'
import { useEffect } from 'react'
import { ThemeProvider, CssBaseline } from '@mui/material'
import { Provider } from 'react-redux'
import type { AppProps } from 'next/app'

import { RouteGuard } from '../components'
import { createEmotionCache } from '../utility'

import { store } from '../state/store'

import { lightTheme } from '../styles'
import '../styles/globals.css'

type AppPropsRoot = AppProps & { emotionCache: EmotionCache }
const clientSideEmotionCache = createEmotionCache()

function MyApp({ Component, emotionCache = clientSideEmotionCache, pageProps }: AppPropsRoot) {
  useEffect(() => {}, [])
  return (
    <Provider store={store}>
      <CacheProvider value={emotionCache}>
        <ThemeProvider theme={lightTheme}>
          <CssBaseline />
            <RouteGuard>
              <Component {...pageProps} />
            </RouteGuard>
        </ThemeProvider>
      </CacheProvider>
    </Provider>
  )
}

export default MyApp
