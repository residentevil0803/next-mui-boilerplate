import axios from 'axios'
import { parseCookies } from 'nookies'
import { COOKIES } from '../utility'

export const getClient = (baseURL: string) => {
  const instance = axios.create({
    baseURL: baseURL,
    timeout: 10000
  })

  // instance.interceptors.request.use(
  //   async function (config) {
  //     // const cookies = await parseCookies()
  //     // if (cookies[COOKIES.JWT]) {
  //     //   config.headers!.Authorization = `Bearer ${cookies[COOKIES.JWT]}`
  //     // }
  //     return { ...config }
  //   },
  //   function (error) {
  //     return Promise.reject(error)
  //   }
  // )

  // instance.interceptors.response.use(
  //   function (response) {
  //     return response
  //   },
  //   function (error) {
  //     return Promise.reject(error)
  //   }
  // )

  return instance
}