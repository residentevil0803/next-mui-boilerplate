import { createSlice } from '@reduxjs/toolkit'

export interface IUser {
  id: string
  emailAddres: string
  password: string
}

const userSlice = createSlice({
  name: 'user',
  initialState: {
    user: {} as IUser,
    
    loggingIn: false,
    loggedIn: false,

    gettingMe: false,
    gotMe: false,

    gettingUser: false,
    gotUser: false,

    error: Object
  },
  reducers: {
    logIn(state, action) {
      state.loggingIn = true,
      state.loggedIn = false
    },
    logInSuccess(state, action) {
      state.loggingIn = true,
      state.loggedIn = true
    },
    logInFailure(state, action) {
      console.log("Failure: ", action.payload)
      state.loggingIn = false,
      state.loggedIn = false,
      state.error = action.payload
    },

    getMe(state) {
      state.gettingMe = true,
      state.gotMe = false
    },
    getMeSuccess(state, action) {
      state.gettingMe = false,
      state.gotMe = true,
      state.user = Object.assign(state.user, action.payload)
    },
    getMeFailure(state, action) {
      state.gettingMe = false,
      state.gotMe = false,
      state.error = action.payload
    },
  }
})

export const {
  logIn,
  logInSuccess,
  logInFailure,
  
  getMe,
  getMeSuccess,
  getMeFailure
} = userSlice.actions

export default userSlice.reducer