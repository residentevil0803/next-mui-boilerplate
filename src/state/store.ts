import { combineReducers, configureStore } from '@reduxjs/toolkit'
import { createRouterMiddleware, initialRouterState, routerReducer } from 'connected-next-router'
import Router from 'next/router'
import createSagaMiddleware from 'redux-saga'

import rootSaga from './saga';
import userReducer from './slice/userSlice'

const routerMiddleware = createRouterMiddleware()
const sagaMiddleware = createSagaMiddleware()
const { asPath } = Router.router || {}

const reducer = combineReducers({
  router: routerReducer,
  user: userReducer
})

export const store = configureStore({
  preloadedState: {
    router: initialRouterState(asPath)
  },
  reducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({ serializableCheck: false, thunk: false })
      .concat(routerMiddleware)
      .concat(sagaMiddleware)
})
sagaMiddleware.run(rootSaga);

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch