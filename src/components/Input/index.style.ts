import styled from '@emotion/styled'

export const InputStyle = styled('div')(({ theme }: any ) => {
  return {
    width: '100%',

    '.label-container': {
      display: 'flex',
      color: 'red',

      '.label-show': {
        color: 'black',
        marginLeft: '0.5rem',
        fontSize: '1rem'
      }
    },

    '.input-container': {
      marginTop: '0.5rem',
      width: '100%',
      height: '2.5rem'
    },
  }
})