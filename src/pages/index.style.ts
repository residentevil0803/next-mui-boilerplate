import styled from '@emotion/styled'

export const LoginStyle = styled('div')(({ theme }: any ) => {
  return {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',

    '.title-container': {
      marginTop: '5rem',
      fontSize: '2rem',
      fontWeight: 900,
    },

    '.login-container': {
      marginTop: '1rem',
      width: '15rem',
      height: '10rem',

      [theme.breakpoints.up('md')]: {
        width: '25rem'
      },

      '.input-box': {
        marginTop: '1rem'
      },

      '.login-button-container': {
        marginTop: '2rem',
        width: '100%',
        backgroundColor: theme.bg.blue,
        color: theme.text.white,
      }
    },

    '.notice-container': {
      marginTop: '2rem',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',

      '.notice-title': {
        fontSize: '1rem',
        lineHeight: '1rem'
      },

      a: {
        marginTop: '-0.2rem',
        marginLeft: '0.5rem',
        fontSize: '1rem',
        color: theme.text.accentBlue,
      }
    },
  }
})